#ifndef _GETLINE_H
#define _GETLINE_H

#include <cstdint>

#include "nbio.h"

namespace nbcp {

#ifndef GETLINE_BUFFER_SIZE
#define GETLINE_BUFFER_SIZE 256
#endif

#ifndef GETLINE_ECHO
#define GETLINE_ECHO 1
#endif

#ifndef GETLINE_COLOR
#define GETLINE_COLOR 1
#endif

#ifndef GETLINE_ANSI
#define GETLINE_ANSI 1
#endif

class getline {
  input in;
  writer<1> out;
  bool new_line;
  char eol;
  uint16_t len;
  uint16_t out_len;
  uint16_t remainder;
  char *next;
  const char *prompt;
  char inbuf[GETLINE_BUFFER_SIZE];
  char outbuf[GETLINE_BUFFER_SIZE];

  int space();
  char *buffer();
  char *process(uint16_t count);
  void show_prompt(char **out);
  void render(char **out, int len);

public:
  getline(input in, output out, const char *prompt)
      : in(in), out(out), new_line(true), eol(0), len(0), remainder(0), next(0),
        prompt(prompt) {}
  void redraw();
  char *poll();
};

} // namespace nbcp

#endif // _GETLINE_H
