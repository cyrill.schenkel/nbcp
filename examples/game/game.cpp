#if __has_include(<Arduino.h>)
#include <Arduino.h>
#else
#error "only works with framework = arduino"
#endif

#include <cstdint>

#define LED_A D0
#define LED_B D1
#define BTN_A D2
#define BTN_B D3

#define DELAY 500
#define INPUT_DELAY 100

class game {
  bool gap;
  uint8_t btns;
  uint8_t secret;
  uint8_t level;
  uint8_t cursor;
  uint32_t next;
  uint32_t scan;
  enum { STARTED, PREVIEW, ENTRY, WON, LOST } state;

  void poll_input(void);
  void poll_started(void);
  void poll_preview(void);
  void poll_entry(void);
  void poll_won(void);
  void poll_lost(void);
  bool btn_a(void);
  bool btn_b(void);

public:
  game() {
    srand(analogRead(A0));
    reset();
  }
  void reset(void) { reset(rand()); };
  void reset(uint8_t secret);
  void poll(void);
} game;

void setup(void) { Serial.begin(9600); }

void loop(void) { game.poll(); }

void game::reset(uint8_t secret) {
  this->secret = secret;
  state = STARTED;
  level = 1;
  cursor = 0;
  uint32_t now = millis();
  next = now + DELAY;
  scan = now + INPUT_DELAY;
  pinMode(LED_A, OUTPUT);
  pinMode(LED_B, OUTPUT);
  pinMode(BTN_A, INPUT);
  pinMode(BTN_B, INPUT);
  digitalWrite(LED_A, HIGH);
  digitalWrite(LED_B, LOW);
}

void game::poll() {
  poll_input();
  if (btn_a() && btn_b() && state != STARTED) {
    reset();
    return;
  }
  switch (state) {
  case STARTED:
    poll_started();
    break;
  case PREVIEW:
    poll_preview();
    break;
  case ENTRY:
    poll_entry();
    break;
  case WON:
    poll_won();
    break;
  case LOST:
    poll_lost();
    break;
  }
}

bool game::btn_a() { return btns & 0x10; }
bool game::btn_b() { return btns & 0x20; }

void game::poll_input(void) {
  btns &= 0x0F;
  uint32_t now = millis();
  if (now >= scan) {
    scan = now + INPUT_DELAY;
    uint8_t a1 = btns & 1;
    uint8_t b1 = (btns >> 1) & 1;
    uint8_t a2 = digitalRead(BTN_A) == HIGH;
    uint8_t b2 = digitalRead(BTN_B) == HIGH;
    btns = a2 | (b2 << 1);
    if (a1 == 1 && a2 == 0)
      btns |= 0x10;
    if (b1 == 1 && b2 == 0)
      btns |= 0x20;
  }
}

void game::poll_started(void) {
  uint32_t now = millis();
  if (now >= next) {
    next = now + DELAY;
    digitalWrite(LED_A, !digitalRead(LED_A));
    digitalWrite(LED_B, !digitalRead(LED_B));
  }
  if (btn_a() || btn_b()) {
    digitalWrite(LED_A, LOW);
    digitalWrite(LED_B, LOW);
    level = 1;
    cursor = 0;
    gap = true;
    state = PREVIEW;
  }
}

void game::poll_preview(void) {
  uint32_t now = millis();
  if (now >= next) {
    next = now + DELAY;
    if (gap) {
      gap = false;
      digitalWrite(LED_A, LOW);
      digitalWrite(LED_B, LOW);
      return;
    } else {
      gap = true;
    }
    if (cursor >= level) {
      digitalWrite(LED_A, HIGH);
      digitalWrite(LED_B, HIGH);
      cursor = 0;
      state = ENTRY;
    } else {
      if ((secret >> cursor) & 1) {
        digitalWrite(LED_A, HIGH);
        digitalWrite(LED_B, LOW);
      } else {
        digitalWrite(LED_A, LOW);
        digitalWrite(LED_B, HIGH);
      }
      cursor++;
    }
  }
}

void game::poll_entry(void) {
  if (cursor >= level) {
    level++;
    cursor = 0;
    gap = true;
    if (level > 8) {
      state = WON;
      scan = millis() + DELAY * 4;
    } else {
      state = PREVIEW;
    }
  } else {
    if (digitalRead(BTN_A)) {
      digitalWrite(LED_A, HIGH);
      digitalWrite(LED_B, LOW);
    }
    if (btn_a()) {
      digitalWrite(LED_A, LOW);
      digitalWrite(LED_B, LOW);
      if ((~secret >> cursor) & 1) {
        state = LOST;
        scan = millis() + DELAY * 4;
      }
      cursor++;
    }
    if (digitalRead(BTN_B)) {
      digitalWrite(LED_A, LOW);
      digitalWrite(LED_B, HIGH);
    }
    if (btn_b()) {
      digitalWrite(LED_A, LOW);
      digitalWrite(LED_B, LOW);
      if ((secret >> cursor) & 1) {
        state = LOST;
        scan = millis() + DELAY * 4;
      }
      cursor++;
    }
  }
}

void game::poll_won(void) {
  uint32_t now = millis();
  if (now >= next) {
    next = now + DELAY;
    digitalWrite(LED_A, LOW);
    digitalWrite(LED_B, !digitalRead(LED_B));
  }
  if (btn_a() || btn_b()) {
    reset();
  }
}

void game::poll_lost(void) {
  uint32_t now = millis();
  if (now >= next) {
    next = now + DELAY;
    digitalWrite(LED_B, LOW);
    digitalWrite(LED_A, !digitalRead(LED_A));
  }
  if (btn_a() || btn_b()) {
    reset();
  }
}
