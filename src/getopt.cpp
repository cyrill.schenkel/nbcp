#include "getopt.h"

#include <cstdio>
#include <cstring>

namespace nbcp {

static inline int is_free(char *arg);

getopt::getopt(int argc, char **argv, const char *options,
               getopt_options_t *long_options)
    : done{}, argc(argc), argv(argv), long_options(long_options),
      options(options), sopts{}, argi(1), unknown{}, arg{} {}

int getopt::next() {
  unknown = 0;
  arg = 0;
  if (done)
    return GETOPT_STATUS_DONE;
  int result = 0;

  while (!result) {
    if (sopts) {
      int opt = *(sopts++);
      if (opt) {
        const char *opts = options;
        while (*opts && !result) {
          if (*opts == opt) {
            result = opt;
            if (*(opts + 1) == ':') {
              if (*sopts) {
                arg = sopts;
                sopts = 0;
              } else if (argi < argc && is_free(argv[argi])) {
                arg = argv[argi++];
              } else {
                unknown = opt;
                result = '?';
                snprintf(error, GETOPT_MAX_ERROR_LENGTH, "-%c missing arg",
                         opt);
              }
            }
          }
          opts++;
        }
        if (!result) {
          result = '?';
          unknown = opt;
          snprintf(error, GETOPT_MAX_ERROR_LENGTH, "-%c unknown option", opt);
        }
      } else {
        sopts = 0;
      }
    } else if (argi >= argc) {
      done = 1;
      result = GETOPT_STATUS_DONE;
    } else if (is_free(argv[argi])) {
      int count = 1;
      while (argi + count < argc && is_free(argv[argi + count]))
        count++;
      if (argi + count >= argc) {
        done = 1;
        result = GETOPT_STATUS_DONE;
      } else {
        char *buf[GETOPT_MAX_FREE_ARG_COUNT];
        memcpy(buf, argv + argi, count * sizeof(char **));
        int a = has_arg(argv[argi + count]);
        memmove(argv + argi, argv + argi + count, (1 + a) * sizeof(char **));
        memcpy(argv + argi + 1 + a, buf, count * sizeof(char **));
      }
    } else if (argv[argi][1] == '-') {
      if (argv[argi][2]) {
        char *name = argv[argi] + 2;
        if (long_options) {
          getopt_options_t *options = long_options;
          while (options->name) {
            if (strcmp(options->name, name) == 0) {
              result = options->value;
              if (options->has_arg) {
                if (argi < argc && is_free(argv[argi + 1])) {
                  arg = argv[++argi];
                } else {
                  unknown = result;
                  result = '?';
                  arg = name;
                  snprintf(error, GETOPT_MAX_ERROR_LENGTH, "--%s missing arg",
                           name);
                }
              }
            }
            options++;
          }
          if (!result) {
            result = '?';
            arg = name;
            snprintf(error, GETOPT_MAX_ERROR_LENGTH, "--%s unknown option",
                     name);
          }
        } else {
          result = '?';
          arg = name;
          snprintf(error, GETOPT_MAX_ERROR_LENGTH, "--%s unknown option", name);
        }
        argi++;
      } else {
        argi++;
        done = 1;
        result = GETOPT_STATUS_DONE;
      }
    } else {
      sopts = argv[argi++] + 1;
    }
  }

  return result;
}

bool getopt::has_arg(char *arg) {
  if (arg[0] == '-') {
    if (arg[1] == '-') {
      char *name = arg + 2;
      getopt_options_t *opts = long_options;
      while (opts->name) {
        if (strcmp(opts->name, name) == 0)
          return opts->has_arg;
        opts++;
      }
    } else {
      arg++;
      while (*arg) {
        const char *opts = options;
        while (*opts) {
          if (opts[0] == *arg) {
            if (opts[1] == ':') {
              return true;
            }
            break;
          }
          opts++;
        }
        arg++;
      }
    }
  }
  return false;
}

static inline int is_free(char *arg) { return arg[0] != '-' || !arg[1]; }

} // namespace nbcp
