#include "nbio.h"

#include <cstring>

namespace nbcp {

int channel::write(const char *buffer, uint16_t count) {
  if (is_closed())
    return NBIO_CHANNEL_CLOSED;
  uint16_t available = write_available();
  count = available < count ? available : count;
  if (count > NBIO_BUFFER_SIZE - write_cur) {
    uint16_t first = NBIO_BUFFER_SIZE - write_cur;
    memcpy(data + write_cur, buffer, first);
    memcpy(data, buffer + first, count - first);
  } else {
    memcpy(data + write_cur, buffer, count);
  }
  write_cur = (write_cur + count) % NBIO_BUFFER_SIZE;
  ready += count;
  return count;
}

int channel::read(char *buffer, uint16_t count) {
  if (is_closed())
    return NBIO_CHANNEL_CLOSED;
  uint16_t available = read_available();
  count = available < count ? available : count;
  if (count > NBIO_BUFFER_SIZE - read_cur) {
    uint16_t first = NBIO_BUFFER_SIZE - read_cur;
    memcpy(buffer, data + read_cur, first);
    memcpy(buffer + first, data, count - first);
  } else {
    memcpy(buffer, data + read_cur, count);
  }
  read_cur = (read_cur + count) % NBIO_BUFFER_SIZE;
  ready -= count;
  return count;
}

int input::read(output &out) {
  int r = available();
  int w = out.available();
  int count = r > w ? w : r;
  if (count == 0)
    return 0;
  count = count > NBIO_BUFFER_SIZE ? NBIO_BUFFER_SIZE : count;
  char buffer[NBIO_BUFFER_SIZE];
  read(buffer, count);
  out.write(buffer, count);
  return count;
}

int output::write(input &in) {
  int w = available();
  int r = in.available();
  int count = r > w ? w : r;
  if (count == 0)
    return 0;
  count = count > NBIO_BUFFER_SIZE ? NBIO_BUFFER_SIZE : count;
  char buffer[NBIO_BUFFER_SIZE];
  in.read(buffer, count);
  write(buffer, count);
  return count;
}

} // namespace nbcp
