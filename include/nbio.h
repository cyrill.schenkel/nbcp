#ifndef _NBIO_H
#define _NBIO_H

#if __has_include(<Arduino.h>)
#include <Arduino.h>
#endif

#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>

#include "queue.h"

namespace nbcp {

#ifndef NBIO_BUFFER_SIZE
#define NBIO_BUFFER_SIZE 32
#endif

#define NBIO_CHANNEL_CLOSED -1

class stream {
  bool closed;

public:
  stream() : closed(false) {}
  bool is_closed() { return closed; }
  void close() { closed = true; }
  virtual int write_available() = 0;
  virtual int read_available() = 0;
  virtual int write(const char *buffer, uint16_t count) = 0;
  virtual int read(char *buffer, uint16_t count) = 0;
};

class channel : public stream {
  uint16_t ready;
  uint16_t read_cur;
  uint16_t write_cur;
  char data[NBIO_BUFFER_SIZE];

public:
  channel() : stream(), ready(0), read_cur(0), write_cur(0) {}
  int write_available() override { return NBIO_BUFFER_SIZE - ready; }
  int read_available() override { return ready; }
  int write(const char *buffer, uint16_t count) override;
  int read(char *buffer, uint16_t count) override;
};

class output;
class input;

class input {
  stream &inner;

public:
  input(stream &inner) : inner(inner) {}
  bool is_closed() { return inner.is_closed(); }
  int available() { return inner.read_available(); }
  int read(char *buffer, uint16_t count) { return inner.read(buffer, count); }
  int read(output &out);
};

class output {
  stream &inner;

public:
  output(stream &inner) : inner(inner) {}
  bool is_closed() { return inner.is_closed(); }
  int available() { return inner.write_available(); }
  int write(const char *buffer, uint16_t count) {
    return inner.write(buffer, count);
  }
  int write(input &in);
};

template <size_t S = 16> class writer {
  struct buffer {
    bool heap;
    size_t count;
    const char *data;
  };
  size_t i;
  buffer *current;
  queue<buffer, S, QUEUE_POLICY_REJECT> buffers;
  output inner;

public:
  writer(stream &inner) : inner(inner), i(0), current(0) {}
  writer(output inner) : inner(inner), i(0), current(0) {}

  ~writer() {
    if (current && current->heap)
      delete[] current->data;
    while ((current = buffers.deq()))
      if (current->heap)
        delete[] current->data;
  }

  bool write(const char *buffer) { return write(buffer, strlen(buffer)); }

  bool write(const char *buffer, uint16_t count) {
    return buffers.enq({
        .heap = false,
        .count = count,
        .data = buffer,
    });
  }

  bool printf(const char *format, ...) {
    va_list args;
    va_start(args, format);
    size_t size = vsnprintf(NULL, 0, format, args);
    va_end(args);
    char *buffer = new char[size];
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);
    bool r = buffers.enq({
        .heap = true,
        .count = size,
        .data = buffer,
    });
    if (!r)
      delete[] buffer;
    return r;
  }

  bool poll() {
    if (inner.is_closed()) {
      if (current && current->heap)
        delete[] current->data;
      while ((current = buffers.deq()))
        if (current->heap)
          delete[] current->data;
      return true;
    }
    if (current == 0) {
      current = buffers.deq();
      i = 0;
    }
    while (current) {
      if (current->count == 0) {
        if (current->heap)
          delete[] current->data;
        current = buffers.deq();
        i = 0;
        if (current == 0)
          return true;
      }
      size_t c = inner.write(current->data + i, current->count);
      current->count -= c;
      i += c;
      if (current->count)
        return false;
    }
    return true;
  }
};

#if __has_include(<Arduino.h>)

class arduino_channel : public stream {
  Stream &inner;

  int write_available() override { return inner.availableForWrite(); }
  int read_available() override { return inner.available(); }

  int write(const char *buffer, uint16_t count) override {
    if (is_closed())
      return NBIO_CHANNEL_CLOSED;
    size_t available = inner.availableForWrite();
    count = available < count ? available : count;
    return inner.write(buffer, count);
  }

  int read(char *buffer, uint16_t count) override {
    if (is_closed())
      return NBIO_CHANNEL_CLOSED;
    size_t available = inner.available();
    count = available < count ? available : count;
    return inner.readBytes(buffer, count);
  }

public:
  arduino_channel(Stream &inner) : stream(), inner(inner) {}
};

#endif

} // namespace nbcp

#endif // _NBIO_H
