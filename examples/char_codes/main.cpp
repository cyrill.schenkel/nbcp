#if __has_include(<Arduino.h>)
#include <Arduino.h>
#else
#error "only works with framework = arduino"
#endif

void setup() { Serial.begin(9600); }

void loop() {
  if (Serial.available()) {
    Serial.printf("%d ", Serial.read());
  }
}
