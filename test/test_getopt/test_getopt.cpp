#include "argparse.h"
#include "getopt.h"
#include "unity.h"

void setUp(void) {}

void tearDown(void) {}

void test_getopt_should_parseSingleOption(void) {
  char line[] = "app -o";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "o", 0);
  TEST_ASSERT_EQUAL_CHAR('o', opt.next());
}

void test_getopt_should_parseMultipleOptions(void) {
  char line[] = "app -o -bc";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "obc", 0);
  TEST_ASSERT_EQUAL_CHAR('o', opt.next());
  TEST_ASSERT_EQUAL_CHAR('b', opt.next());
  TEST_ASSERT_EQUAL_CHAR('c', opt.next());
}

void test_getopt_should_detectUnknownOption(void) {
  char line[] = "app -hjklxyz";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "hl", 0);
  TEST_ASSERT_EQUAL_CHAR('h', opt.next());
  TEST_ASSERT_EQUAL_CHAR('?', opt.next());
  TEST_ASSERT_EQUAL_CHAR('j', opt.unknown);
  TEST_ASSERT_EQUAL_CHAR('?', opt.next());
  TEST_ASSERT_EQUAL_CHAR('k', opt.unknown);
  TEST_ASSERT_EQUAL_CHAR('l', opt.next());
  TEST_ASSERT_EQUAL_CHAR('?', opt.next());
  TEST_ASSERT_EQUAL_CHAR('x', opt.unknown);
  TEST_ASSERT_EQUAL_CHAR('?', opt.next());
  TEST_ASSERT_EQUAL_CHAR('y', opt.unknown);
  TEST_ASSERT_EQUAL_CHAR('?', opt.next());
  TEST_ASSERT_EQUAL_CHAR('z', opt.unknown);
}

void test_getopt_should_stopParsingAfterDoubleDash(void) {
  char line[] = "app -ab -cd -- -x";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "abcd", 0);
  TEST_ASSERT_EQUAL_CHAR('a', opt.next());
  TEST_ASSERT_EQUAL_CHAR('b', opt.next());
  TEST_ASSERT_EQUAL_CHAR('c', opt.next());
  TEST_ASSERT_EQUAL_CHAR('d', opt.next());
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
}

void test_getopt_should_findRequiredArg(void) {
  char line[] = "app -x abc -y";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "x:y", 0);
  TEST_ASSERT_EQUAL_CHAR('x', opt.next());
  TEST_ASSERT_NOT_NULL(opt.arg);
  TEST_ASSERT_EQUAL_STRING("abc", opt.arg);
  TEST_ASSERT_EQUAL_CHAR('y', opt.next());
}

void test_getopt_should_findRequiredArgWithoutSpace(void) {
  char line[] = "app -x2 -x 3";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "x:", 0);
  TEST_ASSERT_EQUAL_CHAR('x', opt.next());
  TEST_ASSERT_NOT_NULL(opt.arg);
  TEST_ASSERT_EQUAL_STRING("2", opt.arg);
  TEST_ASSERT_EQUAL_CHAR('x', opt.next());
  TEST_ASSERT_NOT_NULL(opt.arg);
  TEST_ASSERT_EQUAL_STRING("3", opt.arg);
}

void test_getopt_should_findMultipleRequiredArgs(void) {
  char line[] = "app -x abc -yx xyz";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "x:y", 0);
  TEST_ASSERT_EQUAL_CHAR('x', opt.next());
  TEST_ASSERT_EQUAL_STRING("abc", opt.arg);
  TEST_ASSERT_EQUAL_CHAR('y', opt.next());
  TEST_ASSERT_EQUAL_CHAR('x', opt.next());
  TEST_ASSERT_EQUAL_STRING("xyz", opt.arg);
}

void test_getopt_should_allowEscapedSpacesInArg(void) {
  char line[] = "app -x abc\\ def";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "x:", 0);
  TEST_ASSERT_EQUAL_CHAR('x', opt.next());
  TEST_ASSERT_EQUAL_STRING("abc def", opt.arg);
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
}

void test_getopt_should_detectMissingRequiredArg(void) {
  char line[] = "app -abc";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "abc:", 0);
  TEST_ASSERT_EQUAL_CHAR('a', opt.next());
  TEST_ASSERT_EQUAL_CHAR('b', opt.next());
  TEST_ASSERT_EQUAL_CHAR('?', opt.next());
  TEST_ASSERT_EQUAL_CHAR('c', opt.unknown);
  TEST_ASSERT_NULL(opt.arg);
}

void test_getopt_should_moveFreeArgsBack(void) {
  char line[] = "app foo bar -a xyz";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "a:", 0);
  TEST_ASSERT_EQUAL_CHAR('a', opt.next());
  TEST_ASSERT_EQUAL_STRING("xyz", opt.arg);
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
  TEST_ASSERT_EQUAL_STRING("foo", argv[opt.argi]);
  TEST_ASSERT_EQUAL_STRING("bar", argv[opt.argi + 1]);
}

void test_getopt_should_moveFreeArgsBackAndKeepOrder(void) {
  char line[] = "app foo bar -a xyz";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "a", 0);
  TEST_ASSERT_EQUAL_CHAR('a', opt.next());
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
  TEST_ASSERT_EQUAL_STRING("foo", argv[opt.argi]);
  TEST_ASSERT_EQUAL_STRING("bar", argv[opt.argi + 1]);
  TEST_ASSERT_EQUAL_STRING("xyz", argv[opt.argi + 2]);
}

void test_getopt_should_parseQuotedArgs(void) {
  char line[] = "app -a \"hello, world!\" \"free args\" -a 'single'";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt opt(argc, argv, "a:", 0);
  TEST_ASSERT_EQUAL_CHAR('a', opt.next());
  TEST_ASSERT_EQUAL_STRING("hello, world!", opt.arg);
  TEST_ASSERT_EQUAL_CHAR('a', opt.next());
  TEST_ASSERT_EQUAL_STRING("single", opt.arg);
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
  TEST_ASSERT_EQUAL_STRING("free args", argv[opt.argi]);
}

void test_getopt_should_parseLongOption(void) {
  char line[] = "app --verbose";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt_options_t options[] = {
      {.name = "verbose", .has_arg = false, .value = 'v'}, {}};
  nbcp::getopt opt(argc, argv, "", options);
  TEST_ASSERT_EQUAL_CHAR('v', opt.next());
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
}

void test_getopt_should_parseLongOptionWithArg(void) {
  char line[] = "app -v --time 2 --time 23";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt_options_t options[] = {
      {.name = "time", .has_arg = true, .value = 't'}, {}};
  nbcp::getopt opt(argc, argv, "v", options);
  TEST_ASSERT_EQUAL_CHAR('v', opt.next());
  TEST_ASSERT_EQUAL_CHAR('t', opt.next());
  TEST_ASSERT_EQUAL_STRING("2", opt.arg);
  TEST_ASSERT_EQUAL_CHAR('t', opt.next());
  TEST_ASSERT_EQUAL_STRING("23", opt.arg);
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
}

void test_getopt_should_parseLongOptionsWithArgs(void) {
  char line[] = "app --blink 20 --delay 200";
  char *argv[32];
  int argc = nbcp::argparse(line, 32, argv);
  nbcp::getopt_options_t options[] = {
      {"on", false, 'i'},
      {"off", false, 'o'},
      {"blink", true, 'b'},
      {"delay", true, 'd'},
      {},
  };
  nbcp::getopt opt(argc, argv, "iob:d:", options);
  TEST_ASSERT_EQUAL_CHAR('b', opt.next());
  TEST_ASSERT_EQUAL_STRING("20", opt.arg);
  TEST_ASSERT_EQUAL_CHAR('d', opt.next());
  TEST_ASSERT_EQUAL_STRING("200", opt.arg);
  TEST_ASSERT_EQUAL(GETOPT_STATUS_DONE, opt.next());
}

int runUnityTests(void) {
  UNITY_BEGIN();
  RUN_TEST(test_getopt_should_parseSingleOption);
  RUN_TEST(test_getopt_should_parseMultipleOptions);
  RUN_TEST(test_getopt_should_detectUnknownOption);
  RUN_TEST(test_getopt_should_stopParsingAfterDoubleDash);
  RUN_TEST(test_getopt_should_findRequiredArg);
  RUN_TEST(test_getopt_should_findRequiredArgWithoutSpace);
  RUN_TEST(test_getopt_should_findMultipleRequiredArgs);
  RUN_TEST(test_getopt_should_allowEscapedSpacesInArg);
  RUN_TEST(test_getopt_should_detectMissingRequiredArg);
  RUN_TEST(test_getopt_should_moveFreeArgsBack);
  RUN_TEST(test_getopt_should_moveFreeArgsBackAndKeepOrder);
  RUN_TEST(test_getopt_should_parseQuotedArgs);
  RUN_TEST(test_getopt_should_parseLongOption);
  RUN_TEST(test_getopt_should_parseLongOptionWithArg);
  RUN_TEST(test_getopt_should_parseLongOptionsWithArgs);
  return UNITY_END();
}

#if __has_include(<Arduino.h>)

#include <Arduino.h>

void setup() {
  // Wait ~2 seconds before the Unity test runner
  // establishes connection with a board Serial interface
  delay(2000);

  runUnityTests();
}

void loop() {}

#elif __has_include("esp_idf_version.h")

void app_main() { runUnityTests(); }

#else

int main(void) { return runUnityTests(); }

#endif
