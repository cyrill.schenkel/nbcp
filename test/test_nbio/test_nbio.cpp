#include "nbio.h"
#include "queue.h"
#include "unity.h"

void setUp(void) {}

void tearDown(void) {}

void test_channel_read_available_should_reportBufferSizeWhenEmpty(void) {
  nbcp::channel c;
  TEST_ASSERT_EQUAL(NBIO_BUFFER_SIZE, c.write_available());
  TEST_ASSERT_EQUAL(0, c.read_available());
}

void test_channel_write_read_shouldRetainOriginalMessage(void) {
  nbcp::channel c;
  c.write("hello", sizeof("hello"));
  TEST_ASSERT_EQUAL(sizeof("hello"), c.read_available());
  char msg[sizeof("hello") + 1];
  msg[sizeof("hello")] = 0;
  c.read(msg, sizeof("hello"));
  TEST_ASSERT_EQUAL(0, c.read_available());
  TEST_ASSERT_EQUAL_STRING("hello", msg);
}

void test_writer_write_should_writeMessage(void) {
  nbcp::channel c;
  nbcp::writer<2> w(c);
  TEST_ASSERT_TRUE(w.write("abc"));
  TEST_ASSERT_TRUE(w.write("def"));
  TEST_ASSERT_FALSE(w.write("ghi"));
  TEST_ASSERT_TRUE(w.poll());
  char buffer[7]{};
  c.read(buffer, 6);
  TEST_ASSERT_EQUAL_STRING("abcdef", buffer);
}

int runUnityTests(void) {
  UNITY_BEGIN();
  RUN_TEST(test_channel_read_available_should_reportBufferSizeWhenEmpty);
  RUN_TEST(test_channel_write_read_shouldRetainOriginalMessage);
  RUN_TEST(test_writer_write_should_writeMessage);
  return UNITY_END();
}

#if __has_include(<Arduino.h>)

#include <Arduino.h>

void setup() {
  // Wait ~2 seconds before the Unity test runner
  // establishes connection with a board Serial interface
  delay(2000);

  runUnityTests();
}

void loop() {}

#elif __has_include("esp_idf_version.h")

void app_main() { runUnityTests(); }

#else

int main(void) { return runUnityTests(); }

#endif
