#ifndef _SHELL_H
#define _SHELL_H

#if __has_include(<Arduino.h>)
#include <Arduino.h>
#endif

#include <type_traits>

#include "getline.h"
#include "nbio.h"

namespace nbcp {

#ifndef SHELL_MAX_ARGS
#define SHELL_MAX_ARGS 64
#endif

#ifndef SHELL_MAX_RUNNING
#define SHELL_MAX_RUNNING 8
#endif

#ifndef SHELL_POLL_PER_CYCLE
#define SHELL_POLL_PER_CYCLE 2
#endif

#define SHELL_PROCESS_LIMIT_REACHED -10
#define SHELL_UNKNOWN_COMMAND -11
#define SHELL_NO_COMMAND -12

#define SHELL_SRC_STDIN -1
#define SHELL_SINK_STDOUT -1

typedef int pid_t;

struct poll_t {
  enum { PENDING, READY } status;
  int code;
};

class command {
  channel ch_in;
  channel ch_out;

protected:
  input in;
  output out;

public:
  output in_src;
  input out_sink;
  pid_t src;
  pid_t sink;

  command()
      : ch_in(), ch_out(), in(ch_in), out(ch_out), in_src(ch_in),
        out_sink(ch_out), src(SHELL_SRC_STDIN), sink(SHELL_SINK_STDOUT) {}
  virtual poll_t poll() { return (poll_t){.status = nbcp::poll_t::READY}; }
  virtual int kill() { return 0; }
};

class command_runner {
  size_t next;

protected:
  pid_t foreground;
  input in;
  output out;
  command *running[SHELL_MAX_RUNNING];

  command_runner(input in, output out)
      : foreground(SHELL_NO_COMMAND), next(0), in(in), out(out) {}
  pid_t alloc_pid();
  int stop(pid_t pid);
  virtual void free(command *cmd){};

public:
  bool in_foreground() { return foreground >= 0; }
  void poll();
  virtual pid_t run(char *line) { return 0; }
};

class shell_command_runner : public command_runner {
protected:
  virtual pid_t run_cmd(pid_t pid, int argc, char **argv);

public:
  shell_command_runner(input in, output out) : command_runner(in, out) {}
  using command_runner::run;
  pid_t run(char *line) override;
};

enum shell_op {
  SEQ,
  PIPE,
  IN,
  OUT,
  BG,
  AND,
  OR,
};

class shell_parser {
  char *line;

public:
  shell_op op;
  shell_parser(char *line) : op(SEQ), line(line) {}
  char *next();
};

template <class T> class shell {
#if __has_include(<Arduino.h>)
  arduino_channel ios;
#endif
  input in;
  output out;
  writer<> w;
  T runner;
  getline reader;

public:
  shell(stream &ios, const char *prompt)
      : in(ios), out(ios), w(out), runner(in, out), reader(in, out, prompt) {
    static_assert(::std::is_base_of<command_runner, T>());
  }

#if __has_include(<Arduino.h>)
  shell(Stream &aios, const char *prompt)
      : ios(aios), in(ios), out(ios), w(out), runner(in, out),
        reader(in, out, prompt) {}
#endif

  void poll() {
    if (!w.poll())
      return;
    if (!runner.in_foreground()) {
      char *line = reader.poll();
      if (line && *line) {
        pid_t pid = runner.run(line);
        switch (pid) {
        case SHELL_PROCESS_LIMIT_REACHED:
          w.printf("error: limit of %d processes reached\r\n",
                   SHELL_MAX_RUNNING);
          break;
        case SHELL_UNKNOWN_COMMAND:
          w.write("error: command not found\r\n");
          break;
        }
      }
    }
    runner.poll();
  }
};

} // namespace nbcp

#endif // _SHELL_H
