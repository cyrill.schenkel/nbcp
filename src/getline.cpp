#include <cstring>

#include "getline.h"
#include "nbio.h"

namespace nbcp {

int getline::space() { return GETLINE_BUFFER_SIZE - len; }

char *getline::buffer() {
  if (next != 0) {
    if (remainder > 0) {
      memmove(inbuf, next, remainder);
    }
    len = 0;
    next = 0;
    new_line = true;
  }
  return inbuf + len + remainder;
}

char *getline::process(uint16_t count) {
  count += remainder;
  char *begin = inbuf + len;

  char *obuf = outbuf;

  if (new_line) {
    *(obuf++) = '\r';
    show_prompt(&obuf);
    new_line = false;
  }

  char *result = 0;

  while (count > 0 || result) {
    switch (*begin) {
    case 010:
      if (len < 1) {
        memmove(begin, begin + 1, count - 1);
        len--;
        begin--;
        break;
      }
      memmove(begin - 1, begin + 1, count - 1);
      len -= 2;
      begin -= 2;
#if GETLINE_ANSI
      *(obuf++) = 033;
      *(obuf++) = '[';
      *(obuf++) = '1';
      *(obuf++) = 'D';
      *(obuf++) = 033;
      *(obuf++) = '[';
      *(obuf++) = 'K';
#else
      *(obuf++) = 010;
      *(obuf++) = ' ';
      *(obuf++) = 010;
#endif
      break;
    case '\f':
      obuf = outbuf;
#if GETLINE_ANSI
      *(obuf++) = 033;
      *(obuf++) = '[';
      *(obuf++) = '2';
      *(obuf++) = 'J';
      *(obuf++) = 033;
      *(obuf++) = '[';
      *(obuf++) = 'H';
#else
      *(obuf++) = '\r';
#endif
      render(&obuf, begin - inbuf);
      if (count > 1) {
        memmove(begin, begin + 1, count - 1);
      }
      len--;
      begin--;
      break;
    case '\r':
      if (!eol)
        eol = '\r';
      if (eol == '\r') {
        *begin = 0;
        int skip = 1;
        if (begin[1] == '\n') {
          begin[1] = 0;
        }
        next = begin + skip;
        if (count > skip)
          remainder = count - skip;
        *(obuf++) = '\n';
        *obuf = 0;
        out.write(outbuf, obuf - outbuf);
        return inbuf;
      } else {
        memmove(begin, begin + 1, count - 1);
        len--;
        begin--;
      }
      break;
    case '\n':
      if (!eol)
        eol = '\n';
      if (eol == '\n') {
        *begin = 0;
        int skip = 1;
        if (begin[1] == '\r') {
          begin[1] = 0;
        }
        next = begin + skip;
        if (count > skip)
          remainder = count - skip;
        *(obuf++) = '\n';
        *obuf = 0;
        out.write(outbuf, obuf - outbuf);
        return inbuf;
      } else {

        memmove(begin, begin + 1, count - 1);
        len--;
        begin--;
        break;
      }
    default:
#if GETLINE_ECHO
      *(obuf++) = *begin;
#endif
      break;
    }
    len++;
    count--;
    begin++;
  }
  *obuf = 0;

  out.write(outbuf, obuf - outbuf);

  return 0;
}

void getline::redraw() {
  char *buf = outbuf;
  render(&buf, len);
  out.write(buf, buf - outbuf);
}

char *getline::poll() {
  int spc = space();
  char *buf = buffer();
  uint16_t count = in.read(buf, spc);
  char *line = process(count);
  while (!out.poll())
    ;
  return line;
}

void getline::show_prompt(char **out) {
  int len = strlen(prompt);
  memcpy(*out, prompt, len);
  *out += len;
}

void getline::render(char **out, int count) {
  show_prompt(out);
  memcpy(*out, inbuf, count);
  *out += count;
  **out = 0;
}

} // namespace nbcp
