#ifndef _QUEUE_H
#define _QUEUE_H

#include <cstddef>

#define QUEUE_POLICY_REJECT 0
#define QUEUE_POLICY_DROP_OLDEST 1

namespace nbcp {

template <typename T, size_t S, int P = QUEUE_POLICY_REJECT> class queue {
  size_t first;
  size_t size;
  T data[S];

public:
  queue() : first(0), size(0) {}

  bool enq(T item) {
    size_t next = (first + size) % S;
    if (size == S) {
      if (P == QUEUE_POLICY_REJECT) {
        return false;
      } else {
        data[next] = item;
        first++;
      }
    } else {
      data[next] = item;
      size++;
    }
    return true;
  }

  T *deq() {
    if (size == 0)
      return 0;
    T *item = data + first;
    first = (first + 1) % S;
    size--;
    return item;
  }
};

} // namespace nbcp

#endif // _QUEUE_H
