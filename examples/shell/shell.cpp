#if __has_include(<Arduino.h>)
#include <Arduino.h>
#else
#error "only works with framework = arduino"
#endif

#include <cstdint>
#include <cstring>

#define MAX_ARGS 64

#define BLINK_DELAY 750

#include "getopt.h"
#include "shell.h"

class example_command_runner : public nbcp::shell_command_runner {
  pid_t run_cmd(pid_t pid, int argc, char **argv) override;
  void free(nbcp::command *cmd) override;

public:
  example_command_runner(nbcp::input in, nbcp::output out)
      : shell_command_runner(in, out) {}
};

class cmd_led : public nbcp::command {
  bool error;
  bool on;
  bool verbose;
  int blink_times;
  uint32_t last_blink;
  uint32_t delay;
  nbcp::writer<> writer;

  nbcp::poll_t poll() override;

public:
  cmd_led(int argc, char **argv);
};

nbcp::shell<example_command_runner> sh(Serial, "sh$ ");

void setup() { Serial.begin(9600); }

void loop() { sh.poll(); }

pid_t example_command_runner::run_cmd(pid_t pid, int argc, char **argv) {
  if (strcmp("led", argv[0]) == 0) {
    running[pid] = new cmd_led(argc, argv);
  } else {
    return SHELL_UNKNOWN_COMMAND;
  }
  return pid;
}

void example_command_runner::free(nbcp::command *cmd) { delete cmd; }

cmd_led::cmd_led(int argc, char **argv)
    : command(), error(false), on(false), verbose(false), blink_times(0),
      last_blink(0), delay(BLINK_DELAY), writer(out) {
  nbcp::getopt_options_t options[] = {
      {"on", false, 'i'},
      {"off", false, 'o'},
      {"blink", true, 'b'},
      {"delay", true, 'd'},
      {},
  };
  nbcp::getopt opts(argc, argv, "iob:d:v", options);
  int opt;
  while ((opt = opts.next()) != GETOPT_STATUS_DONE) {
    switch (opt) {
    case 'i':
      on = true;
      break;
    case 'o':
      on = false;
      break;
    case 'b':
      blink_times = atoi(opts.arg) * 2;
      break;
    case 'd':
      delay = abs(atoi(opts.arg));
      break;
    case 'v':
      verbose = true;
      break;
    case '?':
      error = true;
      writer.printf("led: %s\r\n", opts.error);
      break;
    }
  }
  pinMode(LED_BUILTIN, OUTPUT);
}

nbcp::poll_t cmd_led::poll() {
  writer.poll();
  if (error)
    return (nbcp::poll_t){.status = nbcp::poll_t::READY, .code = -1};
  uint32_t now = millis();
  if (last_blink + delay <= now) {
    if (on) {
      digitalWrite(LED_BUILTIN, HIGH);
      if (verbose)
        writer.write("led on\r\n");
    } else {
      digitalWrite(LED_BUILTIN, LOW);
      if (verbose)
        writer.write("led off\r\n");
    }
    on = !on;
    last_blink = now;
    blink_times--;
  }
  if (blink_times <= 0)
    return (nbcp::poll_t){.status = nbcp::poll_t::READY};
  else
    return (nbcp::poll_t){.status = nbcp::poll_t::PENDING};
}
