#ifndef _ARGPARSE_H
#define _ARGPARSE_H

namespace nbcp {

#define ARGPARSE_STATUS_TOO_MANY_ARGS -1
#define ARGPARSE_STATUS_INCOMPLETE_QUOTE -2
#define ARGPARSE_STATUS_INCOMPLETE_ESC -3

// Parse line into array of args.
//
// `argparse` might modify `line`.
//
// No more than `max - 1` arguments are parsed.
// Returns the number of parsed arguments or an error code if the arguments
// could not be parsed.
int argparse(char *line, int max, char **argv);

} // namespace nbcp

#endif // _ARGPARSE_H
