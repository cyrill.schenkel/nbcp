#include "argparse.h"
#include "unity.h"

void setUp(void) {}

void tearDown(void) {}

void test_argparse_none(void) {
  char line[] = "";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(0, argc);
}

void test_argparse_padding(void) {
  char line[] = "    ";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(0, argc);
}

void test_argparse_single(void) {
  char line[] = "app";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(1, argc);
  TEST_ASSERT_EQUAL_STRING("app", argv[0]);
}

void test_argparse_multiple(void) {
  char line[] = "foo bar baz";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(3, argc);
  TEST_ASSERT_EQUAL_STRING("foo", argv[0]);
  TEST_ASSERT_EQUAL_STRING("bar", argv[1]);
  TEST_ASSERT_EQUAL_STRING("baz", argv[2]);
}

void test_argparse_singleQuoted(void) {
  char line[] = "'foo bar' baz";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(2, argc);
  TEST_ASSERT_EQUAL_STRING("foo bar", argv[0]);
  TEST_ASSERT_EQUAL_STRING("baz", argv[1]);
}

void test_argparse_quoted(void) {
  char line[] = "foo \"bar baz\"";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(2, argc);
  TEST_ASSERT_EQUAL_STRING("foo", argv[0]);
  TEST_ASSERT_EQUAL_STRING("bar baz", argv[1]);
}

void test_argparse_padded(void) {
  char line[] = "  hello  world  w l    ";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(4, argc);
  TEST_ASSERT_EQUAL_STRING("hello", argv[0]);
  TEST_ASSERT_EQUAL_STRING("world", argv[1]);
  TEST_ASSERT_EQUAL_STRING("w", argv[2]);
  TEST_ASSERT_EQUAL_STRING("l", argv[3]);
}

void test_argparse_quotedEscapes(void) {
  char line[] = "foo \"bar\\n\\r\\\\\\\" baz\" ";
  char *argv[10];
  int argc = nbcp::argparse(line, 10, argv);
  TEST_ASSERT_EQUAL(2, argc);
  TEST_ASSERT_EQUAL_STRING("foo", argv[0]);
  TEST_ASSERT_EQUAL_STRING("bar\n\r\\\" baz", argv[1]);
}

int runUnityTests(void) {
  UNITY_BEGIN();
  RUN_TEST(test_argparse_none);
  RUN_TEST(test_argparse_padding);
  RUN_TEST(test_argparse_single);
  RUN_TEST(test_argparse_multiple);
  RUN_TEST(test_argparse_singleQuoted);
  RUN_TEST(test_argparse_quoted);
  RUN_TEST(test_argparse_padded);
  RUN_TEST(test_argparse_quotedEscapes);
  return UNITY_END();
}

#if __has_include(<Arduino.h>)

#include <Arduino.h>

void setup() {
  // Wait ~2 seconds before the Unity test runner
  // establishes connection with a board Serial interface
  delay(2000);

  runUnityTests();
}

void loop() {}

#elif __has_include("esp_idf_version.h")

void app_main() { runUnityTests(); }

#else

int main(void) { return runUnityTests(); }

#endif
