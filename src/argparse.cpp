#include "argparse.h"

#include <cstring>

namespace nbcp {

static char *parse_quoted(char *text, int *status);

int argparse(char *line, int max, char **argv) {
  char *start = line;
  int status = 0;
  int esc = 0;
  int argc = 0;
  while (argc < max && !status) {
    switch (*line) {
    case 0:
      if (start != line) {
        *(line++) = 0;
        argv[argc++] = start;
      }
      status = argc;
      if (!status)
        return 0;
      break;
    case ' ':
    case '\t':
      if (esc) {
        esc = 0;
        line++;
      } else if (start != line) {
        *(line++) = 0;
        argv[argc++] = start;
        start = line;
      } else {
        start = ++line;
      }
      break;
    case '\'':
    case '"':
      start++;
      line = parse_quoted(line, &status);
      break;
    case '\\':
      memmove(line, line + 1, strlen(line + 1) + 1);
      esc = 1;
    default:
      line++;
      break;
    }
  }

  if (argc == max) {
    return ARGPARSE_STATUS_TOO_MANY_ARGS;
  }

  return status;
}

static char *parse_quoted(char *text, int *status) {
  *status = 0;
  char quote = *text;
  text++;
  int esc = 0;
  while (!*status && *text && (*text != quote || esc)) {
    if (esc) {
      switch (*text) {
      case '\'':
      case '"':
      case '\\':
        text++;
        break;
      case 'n':
        *(text++) = '\n';
        break;
      case 't':
        *(text++) = '\t';
        break;
      case 'r':
        *(text++) = '\r';
        break;
      default:
        *status = ARGPARSE_STATUS_INCOMPLETE_ESC;
      }
      esc = 0;
    } else if (*text == '\\') {
      memmove(text, text + 1, strlen(text + 1) + 1);
      esc = 1;
    } else {
      text++;
    }
  }
  if (*text) {
    *(text++) = 0;
  } else {
    *status = ARGPARSE_STATUS_INCOMPLETE_QUOTE;
  }
  return text;
}

} // namespace nbcp
