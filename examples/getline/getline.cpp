#if __has_include(<Arduino.h>)
#include <Arduino.h>
#else
#error "only works with framework = arduino"
#endif

#include "getline.h"

nbcp::getline input("msg> ");

void setup() { Serial.begin(9600); }

void loop() {
  char *line = input.poll(Serial);
  if (line) {
    Serial.printf("\r\n:    %s\r\n", line);
  }
}
