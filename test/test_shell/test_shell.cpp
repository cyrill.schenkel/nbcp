#include "shell.h"
#include "unity.h"

void setUp(void) {}

void tearDown(void) {}

void test_shell_parser_shouldParseLineWithoutOps(void) {
  char line[] = "ls -lah";
  nbcp::shell_parser p(line);
  TEST_ASSERT_EQUAL_STRING("ls -lah", p.next());
  TEST_ASSERT_EQUAL(nbcp::SEQ, p.op);
  TEST_ASSERT_NULL(p.next());
}

void test_shell_parser_shouldParseLineWithOpAtEnd(void) {
  char line[] = "ls -lah&";
  nbcp::shell_parser p(line);
  TEST_ASSERT_EQUAL_STRING("ls -lah", p.next());
  TEST_ASSERT_EQUAL(nbcp::BG, p.op);
  TEST_ASSERT_NULL(p.next());
}

void test_shell_parser_shouldParseLineWithOpBetween(void) {
  char line[] = "abc || def";
  nbcp::shell_parser p(line);
  TEST_ASSERT_EQUAL_STRING("abc", p.next());
  TEST_ASSERT_EQUAL(nbcp::OR, p.op);
  TEST_ASSERT_EQUAL_STRING("def", p.next());
  TEST_ASSERT_EQUAL(nbcp::SEQ, p.op);
  TEST_ASSERT_NULL(p.next());
}

int runUnityTests(void) {
  UNITY_BEGIN();
  RUN_TEST(test_shell_parser_shouldParseLineWithoutOps);
  RUN_TEST(test_shell_parser_shouldParseLineWithOpAtEnd);
  return UNITY_END();
}

#if __has_include(<Arduino.h>)

#include <Arduino.h>

void setup() {
  // Wait ~2 seconds before the Unity test runner
  // establishes connection with a board Serial interface
  delay(2000);

  runUnityTests();
}

void loop() {}

#elif __has_include("esp_idf_version.h")

void app_main() { runUnityTests(); }

#else

int main(void) { return runUnityTests(); }

#endif
