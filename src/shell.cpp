#include "shell.h"

#include "argparse.h"

namespace nbcp {

pid_t command_runner::alloc_pid() {
  for (pid_t pid = 0; pid < SHELL_MAX_RUNNING; pid++)
    if (!running[pid])
      return pid;
  return SHELL_PROCESS_LIMIT_REACHED;
}

int command_runner::stop(pid_t pid) {
  if (foreground == pid)
    foreground = SHELL_NO_COMMAND;
  command *cmd = running[pid];
  int status = cmd->kill();
  running[pid] = 0;
  free(cmd);
  return status;
}

void command_runner::poll() {
  int remaining = SHELL_MAX_RUNNING;
  int count = SHELL_POLL_PER_CYCLE;
  while (count > 0 && remaining) {
    command *cmd = running[next];
    if (cmd) {
      count--;
      if (cmd->src == SHELL_SRC_STDIN) {
        cmd->in_src.write(in);
      } else {
        // todo
      }
      const poll_t poll = cmd->poll();
      if (cmd->sink == SHELL_SINK_STDOUT) {
        cmd->out_sink.read(out);
      } else {
        // todo
      }
      if (poll.status == poll_t::READY) {
        if (foreground == next)
          foreground = SHELL_NO_COMMAND;
        running[next] = 0;
        free(cmd);
      }
    } else {
      remaining--;
    }
    next = (next + 1) % SHELL_MAX_RUNNING;
  }
}

pid_t shell_command_runner::run(char *line) {
  char *argv[SHELL_MAX_ARGS];
  int argc = nbcp::argparse(line, SHELL_MAX_ARGS, argv);
  if (argc > 0) {
    const pid_t pid = alloc_pid();
    if (pid < 0) {
      return pid;
    } else {
      pid_t pid = run_cmd(pid, argc, argv);
      foreground = pid;
      return pid;
    }
  }
  return SHELL_NO_COMMAND;
}

bool is_opchar(char c) {
  return c == '&' || c == '|' || c == ';' || c == '<' || c == '>';
}

char *shell_parser::next() {
  op = SEQ;
  if (!*line)
    return 0;
  char *result = line;
  while (!is_opchar(*line) && *line != 0)
    line++;
  switch (*line) {
  case '&':
    *(line++) = 0;
    if (line[1] == '&') {
      op = AND;
      line++;
    } else {
      op = BG;
    }
    break;
  case '|':
    *(line++) = 0;
    if (line[1] == '|') {
      op = OR;
      line++;
    } else {
      op = PIPE;
    }
    break;
  case ';':
    *(line++) = 0;
    op = SEQ;
    break;
  case '<':
    *(line++) = 0;
    op = IN;
    break;
  case '>':
    *(line++) = 0;
    op = OUT;
    break;
  }
  return result;
}

} // namespace nbcp
