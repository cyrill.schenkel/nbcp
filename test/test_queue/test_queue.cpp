#include "queue.h"
#include "unity.h"

void setUp(void) {}

void tearDown(void) {}

void test_push_should_addSingleItemToQueue(void) {
  nbcp::queue<int, 10> q;
  q.enq(1);
  const auto v = q.deq();
  TEST_ASSERT_NOT_NULL(v);
  TEST_ASSERT_EQUAL(1, *v);
}

void test_push_shoould_addMultipleItemsToQueue(void) {
  nbcp::queue<int, 4> q;
  q.enq(1);
  q.enq(2);
  q.enq(3);
  q.enq(4);
  TEST_ASSERT_EQUAL(1, *q.deq());
  TEST_ASSERT_EQUAL(2, *q.deq());
  q.enq(5);
  q.enq(6);
  TEST_ASSERT_EQUAL(3, *q.deq());
  TEST_ASSERT_EQUAL(4, *q.deq());
  TEST_ASSERT_EQUAL(5, *q.deq());
  TEST_ASSERT_EQUAL(6, *q.deq());
  TEST_ASSERT_NULL(q.deq());
}

int runUnityTests(void) {
  UNITY_BEGIN();
  RUN_TEST(test_push_should_addSingleItemToQueue);
  return UNITY_END();
}

#if __has_include(<Arduino.h>)

#include <Arduino.h>

void setup() {
  // Wait ~2 seconds before the Unity test runner
  // establishes connection with a board Serial interface
  delay(2000);

  runUnityTests();
}

void loop() {}

#elif __has_include("esp_idf_version.h")

void app_main() { runUnityTests(); }

#else

int main(void) { return runUnityTests(); }

#endif
