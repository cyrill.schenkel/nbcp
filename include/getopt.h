#ifndef _GETOPT_H
#define _GETOPT_H

namespace nbcp {

#ifndef GETOPT_MAX_FREE_ARG_COUNT
#define GETOPT_MAX_FREE_ARG_COUNT 128
#endif

#ifndef GETOPT_MAX_ERROR_LENGTH
#define GETOPT_MAX_ERROR_LENGTH 64
#endif

#define GETOPT_STATUS_DONE -1

typedef struct {
  const char *name;
  bool has_arg;
  int value;
} getopt_options_t;

class getopt {
  int done;
  int argc;
  char **argv;
  getopt_options_t *long_options;
  const char *options;
  char *sopts;

  bool has_arg(char *arg);

public:
  int argi;
  char unknown;
  char *arg;
  char error[GETOPT_MAX_ERROR_LENGTH];

  getopt(int argc, char **argv, const char *options)
      : getopt(argc, argv, options, 0){};
  getopt(int argc, char **argv, const char *options,
         getopt_options_t *long_options);
  int next();
};

} // namespace nbcp

#endif // _GETOPT_H
